# 中文编程（Chinese Program Develop）

### 介绍 
制定、维护中文编程标准，包括关键字、语法、名称等；  
分享中文编程开发软件、语言；  
欢迎进QQ群讨论或提交Issues
  
### 中文IDE大全
不定期更新，详情见网址：http://www.lanurl.com/?thread-96.htm
  
### 中文编程语言大全
不定期更新，详情见网址：http://www.lanurl.com/?thread-128.htm

### [云之梦歌开源协议(DOPL)](https://gitee.com/dcosa/cpl/blob/master/DOPL/DOPL.md)

### 名人堂
[浏览名人堂](https://gitee.com/dcosa/cpl/blob/master/%E4%B8%AD%E6%96%87%E7%BC%96%E7%A8%8B%E5%90%8D%E4%BA%BA%E5%A0%82.md)
  
### [中文编程交流QQ群](https://jq.qq.com/?_wv=1027&k=oNFZGpce)  
欢迎加入讨论。
  